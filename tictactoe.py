#  capture the input player 1
# mark the input into the board
# --> draw
# --> check if the position is
#---> already taken / draw / winner
# player 2


board_game = [
       "[0]","[1]","[2]","\n",
       "[4]","[5]","[6]","\n",
       "[8]","[9]","[10]"
]

player_character = input("Choose your character (X/0): ")
cross = "X"  #Player 1
circle = "O" #Player 2

turn = 0
count = 0
if player_character == "x" or player_character == "X":
    player_character = cross
elif player_character == "o" or player_character == "O":
    player_character = circle




def check_for_winner(mark_type):
    if mark_type == cross:
        player = "Player 1"
    else:
        player = "Player 2"
    if board_game[0] == mark_type and board_game[1] == mark_type and board_game[2] == mark_type:
        print("".join(board_game))
        print(f"We have a winner! {player}")
    elif board_game[4] == mark_type and board_game[5] == mark_type and board_game[6] == mark_type:
        print("".join(board_game))
        print(f"We have a winner! {player}")
    elif board_game[8] == mark_type and board_game[9] == mark_type and board_game[10] == mark_type:
        print("".join(board_game))
        print(f"We have a winner! {player}")
    elif board_game[0] == mark_type and board_game[4] == mark_type and board_game[8] == mark_type:
        print("".join(board_game))
        print(f"We have a winner! {player}")
    elif board_game[1] == mark_type and board_game[5] == mark_type and board_game[9] == mark_type:
        print("".join(board_game))
        print(f"We have a winner! {player}")
    elif board_game[2] == mark_type and board_game[6] == mark_type and board_game[10] == mark_type:
        print("".join(board_game))
        print(f"We have a winner! {player}")
    elif board_game[0] == mark_type and board_game[5] == mark_type and board_game[10] == mark_type:
        print("".join(board_game))
        print(f"We have a winner! {player}")
    elif board_game[2] == mark_type and board_game[5] == mark_type and board_game[8] == mark_type:
        print("".join(board_game))
        print(f"We have a winner! {player}")



def player_turn(mark_type):
  if mark_type == cross:
    player = "Player 1"
  else:
    player = "Player 2"


  player_input = int(input(f"{player} Please enter your position: "))
  board_game[player_input] = mark_type
  check_for_winner(mark_type)
  print("".join(board_game))






